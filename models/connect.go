package models

import (
	"database/sql"
	"log"
)

func connect() *sql.DB {
	// db, err := sql.Open("mysql", "briopr:jakarta123@tcp(10.35.65.136:3306)/mocash")
	db, err := sql.Open("mysql", "root:@tcp(localhost)/user")

	if err != nil {
		log.Fatal(err)
	}

	return db
}
