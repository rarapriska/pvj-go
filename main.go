package main

import (
	"fmt"
	"log"
	"net/http"
	"gitlab.com/rarapriska/pvj-go/models"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func main() {
	log.Println("------- MASUK SINI ---------")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/getusers", returnAllUsers).Methods("GET")
	fmt.Println("Connected to port 321")
	log.Fatal(http.ListenAndServe(":321", router))
}
